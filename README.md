# App

## Core

in *core/* directory:

```sh
make start
make test
```

## Symfony app

in *app/symfony* directory:

```sh
make start
make test
```

## Laravel app

in *app/laravel* directory:

```sh
./vendor/bin/sail up
```

then browse: http://localhost/
