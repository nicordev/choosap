<?php

namespace App\Http\Controllers;

use App\Domain\Poll\Request\SavePollRequest;
use App\Domain\Poll\UseCase\SavePollUseCaseInterface;
use Illuminate\Http\Request;

class SavePollController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SavePollUseCaseInterface $savePollUseCase)
    {
        $savePollRequest = SavePollRequest::createFromArray($request->all());

        $response = $savePollUseCase($savePollRequest);

        dd($response);

        return response(content: $response->getContent(), status: $response->getStatusCode());
    }
}
