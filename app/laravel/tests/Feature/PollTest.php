<?php

namespace Tests\Feature;

use Tests\TestCase;

class PollTest extends TestCase
{
    public function test_admin_can_save_a_post(): void
    {
        $response = $this->post(
            '/api/posts',
            [
                'title' => 'dummy title 1',
                'options' => [],
            ]
        );

        $response->assertStatus(201);
    }

    public function test_admin_cannot_save_an_invalid_post(): void
    {
        $response = $this->post(
            '/api/posts',
            [
                'title' => 'dummy title 1',
            ]
        );

        $response->assertStatus(400);
    }
}
