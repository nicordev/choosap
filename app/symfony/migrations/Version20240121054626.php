<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240121054626 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE choice (
          id VARCHAR(255) NOT NULL,
          poll_id VARCHAR(255) DEFAULT NULL,
          participant VARCHAR(255) NOT NULL,
          PRIMARY KEY(id),
          CONSTRAINT FK_C1AB5A923C947C0F FOREIGN KEY (poll_id) REFERENCES poll (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
        $this->addSql('CREATE INDEX IDX_C1AB5A923C947C0F ON choice (poll_id)');
        $this->addSql('CREATE TABLE option (
          id VARCHAR(255) NOT NULL,
          poll_id VARCHAR(255) NOT NULL,
          choice_id VARCHAR(255) DEFAULT NULL,
          title VARCHAR(255) NOT NULL,
          value VARCHAR(255) NOT NULL,
          PRIMARY KEY(id),
          CONSTRAINT FK_5A8600B03C947C0F FOREIGN KEY (poll_id) REFERENCES poll (id) NOT DEFERRABLE INITIALLY IMMEDIATE,
          CONSTRAINT FK_5A8600B0998666D1 FOREIGN KEY (choice_id) REFERENCES choice (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
        $this->addSql('CREATE INDEX IDX_5A8600B03C947C0F ON option (poll_id)');
        $this->addSql('CREATE INDEX IDX_5A8600B0998666D1 ON option (choice_id)');
        $this->addSql('CREATE TABLE poll (id VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE choice');
        $this->addSql('DROP TABLE option');
        $this->addSql('DROP TABLE poll');
    }
}
