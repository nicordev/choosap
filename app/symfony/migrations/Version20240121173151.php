<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240121173151 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE choice_chosen_option (
          choice_id VARCHAR(255) NOT NULL,
          option_id VARCHAR(255) NOT NULL,
          PRIMARY KEY(choice_id, option_id),
          CONSTRAINT FK_50641D52998666D1 FOREIGN KEY (choice_id) REFERENCES choice (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE,
          CONSTRAINT FK_50641D52A7C41D6F FOREIGN KEY (option_id) REFERENCES option (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
        $this->addSql('CREATE INDEX IDX_50641D52998666D1 ON choice_chosen_option (choice_id)');
        $this->addSql('CREATE INDEX IDX_50641D52A7C41D6F ON choice_chosen_option (option_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__option AS SELECT id, poll_id, title, value FROM option');
        $this->addSql('DROP TABLE option');
        $this->addSql('CREATE TABLE option (
          id VARCHAR(255) NOT NULL,
          poll_id VARCHAR(255) NOT NULL,
          title VARCHAR(255) NOT NULL,
          value VARCHAR(255) NOT NULL,
          PRIMARY KEY(id),
          CONSTRAINT FK_5A8600B03C947C0F FOREIGN KEY (poll_id) REFERENCES poll (id) ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
        $this->addSql('INSERT INTO option (id, poll_id, title, value)
        SELECT
          id,
          poll_id,
          title,
          value
        FROM
          __temp__option');
        $this->addSql('DROP TABLE __temp__option');
        $this->addSql('CREATE INDEX IDX_5A8600B03C947C0F ON option (poll_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE choice_chosen_option');
        $this->addSql('CREATE TEMPORARY TABLE __temp__option AS SELECT id, poll_id, title, value FROM option');
        $this->addSql('DROP TABLE option');
        $this->addSql('CREATE TABLE option (
          id VARCHAR(255) NOT NULL,
          poll_id VARCHAR(255) NOT NULL,
          choice_id VARCHAR(255) DEFAULT NULL,
          title VARCHAR(255) NOT NULL,
          value VARCHAR(255) NOT NULL,
          PRIMARY KEY(id),
          CONSTRAINT FK_5A8600B03C947C0F FOREIGN KEY (poll_id) REFERENCES poll (id) NOT DEFERRABLE INITIALLY IMMEDIATE,
          CONSTRAINT FK_5A8600B0998666D1 FOREIGN KEY (choice_id) REFERENCES choice (id) ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
        $this->addSql('INSERT INTO option (id, poll_id, title, value)
        SELECT
          id,
          poll_id,
          title,
          value
        FROM
          __temp__option');
        $this->addSql('DROP TABLE __temp__option');
        $this->addSql('CREATE INDEX IDX_5A8600B03C947C0F ON option (poll_id)');
        $this->addSql('CREATE INDEX IDX_5A8600B0998666D1 ON option (choice_id)');
    }
}
