<?php

declare(strict_types=1);

namespace App\Domain\Poll\Model\Factory;

use App\Domain\Poll\Model\Choice;

final class ChoiceFactory
{
    /**
     * @param array{'id': string, 'pollId': string, 'participant': string, 'chosenOptionIds': array<string>} $data
     */
    public static function createFromArray(array $data): Choice
    {
        return new Choice(
            id: $data['id'],
            pollId: $data['pollId'],
            participant: $data['participant'],
            chosenOptionIds: $data['chosenOptionIds'],
        );
    }
}
