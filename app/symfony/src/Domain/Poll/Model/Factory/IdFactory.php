<?php

declare(strict_types=1);

namespace App\Domain\Poll\Model\Factory;

final class IdFactory implements IdFactoryInterface
{
    public function create(array $data = []): string
    {
        $payload['data'] = [] === $data
            ? \uniqid(more_entropy: true)
            : $data
        ;

        $payload['createdAt'] = time();

        return \base64_encode(\json_encode($payload));
    }
}
