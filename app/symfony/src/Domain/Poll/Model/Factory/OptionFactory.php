<?php

declare(strict_types=1);

namespace App\Domain\Poll\Model\Factory;

use App\Domain\Poll\Model\Option;

final class OptionFactory
{
    /**
     * @param array<string, string> $data
     */
    public static function createFromArray(array|string $data): Option
    {
        return new Option(
            $data['id'],
            $data['value'],
            $data['title'],
        );
    }
}
