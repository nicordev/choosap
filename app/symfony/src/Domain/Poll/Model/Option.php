<?php

declare(strict_types=1);

namespace App\Domain\Poll\Model;

final class Option
{
    public function __construct(
        private readonly string $id,
        private readonly string $value,
        private readonly string $title,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return array<string, string>
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'value' => $this->getValue(),
            'title' => $this->getTitle(),
        ];
    }
}
