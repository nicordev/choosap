<?php

declare(strict_types=1);

namespace App\Domain\Poll\Repository;

use App\Domain\Poll\Model\Choice;

interface ChooseInterface
{
    public function choose(Choice $choice): void;
}
