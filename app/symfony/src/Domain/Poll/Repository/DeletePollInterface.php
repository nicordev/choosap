<?php

declare(strict_types=1);

namespace App\Domain\Poll\Repository;

interface DeletePollInterface
{
    public function delete(string $id): void;
}
