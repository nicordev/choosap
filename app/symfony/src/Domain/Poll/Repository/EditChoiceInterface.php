<?php

declare(strict_types=1);

namespace App\Domain\Poll\Repository;

interface EditChoiceInterface
{
    /**
     * @param array<string> $chosenOptionIds
     */
    public function editChoice(string $choiceId, array $chosenOptionIds): void;
}
