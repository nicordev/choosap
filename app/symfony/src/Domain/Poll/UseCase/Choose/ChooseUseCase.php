<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\Choose;

use App\Domain\Poll\Model\Factory\ChoiceFactory;
use App\Domain\Poll\Model\Factory\IdFactoryInterface;
use App\Domain\Poll\Repository\ChooseInterface;
use App\Domain\Shared\Response\ResponseInterface;

final class ChooseUseCase implements ChooseUseCaseInterface
{
    public function __construct(
        private readonly ChooseInterface $choiceStore,
        private readonly IdFactoryInterface $idFactory,
    ) {
    }

    public function __invoke(ChooseRequest $chooseRequest): ResponseInterface
    {
        $chooseRequest->validate();
        $id = $this->idFactory->create([$chooseRequest->participant]);

        $choice = ChoiceFactory::createFromArray(['id' => $id, ...$chooseRequest->toArray()]);

        $this->choiceStore->choose($choice);

        return new ChooseResponse($id);
    }
}
