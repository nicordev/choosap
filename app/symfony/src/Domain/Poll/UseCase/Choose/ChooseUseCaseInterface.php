<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\Choose;

use App\Domain\Shared\Response\ResponseInterface;

interface ChooseUseCaseInterface
{
    public function __invoke(ChooseRequest $chooseRequest): ResponseInterface;
}
