<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\DeletePoll;

use App\Domain\Shared\Request\BaseRequest;

final class DeletePollRequest extends BaseRequest
{
    public string $id;
}
