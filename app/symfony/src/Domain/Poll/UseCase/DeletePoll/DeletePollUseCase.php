<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\DeletePoll;

use App\Domain\Poll\Repository\DeletePollInterface;
use App\Domain\Shared\Response\ResponseInterface;

final class DeletePollUseCase implements DeletePollUseCaseInterface
{
    public function __construct(
        private readonly DeletePollInterface $pollStore,
    ) {
    }

    public function __invoke(DeletePollRequest $deletePollRequest): ResponseInterface
    {
        $deletePollRequest->validate();
        $this->pollStore->delete($deletePollRequest->id);

        return new DeletePollResponse();
    }
}
