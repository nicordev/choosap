<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\DeletePoll;

use App\Domain\Shared\Response\ResponseInterface;

interface DeletePollUseCaseInterface
{
    public function __invoke(DeletePollRequest $deletePollRequest): ResponseInterface;
}
