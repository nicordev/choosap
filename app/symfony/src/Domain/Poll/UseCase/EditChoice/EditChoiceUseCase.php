<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\EditChoice;

use App\Domain\Poll\Repository\EditChoiceInterface;
use App\Domain\Shared\Response\ResponseInterface;

final class EditChoiceUseCase implements EditChoiceUseCaseInterface
{
    public function __construct(
        private readonly EditChoiceInterface $choiceStore,
    ) {
    }

    public function __invoke(EditChoiceRequest $editChoiceRequest): ResponseInterface
    {
        $editChoiceRequest->validate();
        $this->choiceStore->EditChoice(choiceId: $editChoiceRequest->choiceId, chosenOptionIds: $editChoiceRequest->chosenOptionIds);

        return new EditChoiceResponse();
    }
}
