<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\ListPolls;

use App\Domain\Shared\Request\BaseRequest;

final class ListPollsRequest extends BaseRequest
{
    public int $offset = 0;
    public int $limit = 10;
}
