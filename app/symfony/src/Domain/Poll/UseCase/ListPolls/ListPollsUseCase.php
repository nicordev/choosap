<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\ListPolls;

use App\Domain\Poll\Repository\FindPollInterface;
use App\Domain\Shared\Response\ResponseInterface;

final class ListPollsUseCase implements ListPollsUseCaseInterface
{
    public function __construct(
        private readonly FindPollInterface $pollStore,
    ) {
    }

    public function __invoke(ListPollsRequest $listPollRequest): ResponseInterface
    {
        return new ListPollsResponse($this->pollStore->find($listPollRequest->offset, $listPollRequest->limit));
    }
}
