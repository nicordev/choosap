<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\SavePoll;

use App\Domain\Poll\Exception\BadRequestException;
use App\Domain\Shared\Request\BaseRequest;

final class SavePollRequest extends BaseRequest
{
    public string $title;
    /**
     * @var array<array{title: string, value: string}>
     */
    public array $options;

    public function validate(): void
    {
        parent::validate();

        $violations = [];

        if ('' === $this->title) {
            $violations[] = 'title is empty';
        }

        if ([] === $this->options) {
            $violations[] = 'options array is empty';
        }

        foreach ($this->options as $option) {
            $optionViolations = $this->validateOption($option);

            if ([] !== $optionViolations) {
                $violations = [...$violations, ...$optionViolations];
                break; // skip other options for performance
            }
        }

        if ([] === $violations) {
            return;
        }

        throw new BadRequestException($violations);
    }

    /**
     * @param array<mixed> $option
     *
     * @return array<string>
     */
    private function validateOption(array $option): array
    {
        $violations = [];

        if (!\array_key_exists('title', $option)) {
            $violations[] = 'missing option title';
        } elseif ('' === $option['title']) {
            $violations[] = 'option title is empty';
        }

        if (!\array_key_exists('value', $option)) {
            $violations[] = 'missing option value';
        } elseif ('' === $option['value']) {
            $violations[] = 'option value is empty';
        }

        return $violations;
    }
}
