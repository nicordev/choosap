<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\SavePoll;

use App\Domain\Poll\Model\Factory\IdFactoryInterface;
use App\Domain\Poll\Model\Factory\PollFactory;
use App\Domain\Poll\Repository\SavePollInterface;
use App\Domain\Shared\Response\ResponseInterface;

final class SavePollUseCase implements SavePollUseCaseInterface
{
    public function __construct(
        private readonly SavePollInterface $pollStore,
        private readonly IdFactoryInterface $idFactory,
    ) {
    }

    public function __invoke(SavePollRequest $savePollRequest): ResponseInterface
    {
        $savePollRequest->validate();
        $id = $this->idFactory->create([$savePollRequest->title]);
        $options = $this->addOptionId($savePollRequest->options);

        $this->pollStore->save(
            PollFactory::createFromArray([
                'id' => $id,
                'title' => $savePollRequest->title,
                'options' => $options,
            ])
        );

        return new SavePollResponse($id);
    }

    /**
     * @param array<array<string>> $options
     *
     * @return array<int, array<int|string, mixed>>
     */
    private function addOptionId(array $options): array
    {
        $optionsWithId = [];

        foreach ($options as $option) {
            $optionsWithId[] = [...$option, 'id' => $this->idFactory->create([$option['value']])];
        }

        return $optionsWithId;
    }
}
