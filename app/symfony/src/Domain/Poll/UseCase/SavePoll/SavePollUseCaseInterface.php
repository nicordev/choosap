<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\SavePoll;

use App\Domain\Shared\Response\ResponseInterface;

interface SavePollUseCaseInterface
{
    public function __invoke(SavePollRequest $savePollRequest): ResponseInterface;
}
