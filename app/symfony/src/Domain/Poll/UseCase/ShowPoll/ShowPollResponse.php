<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\ShowPoll;

use App\Domain\Poll\Model\Option;
use App\Domain\Poll\Model\Poll;
use App\Domain\Shared\Response\ResponseInterface;

final class ShowPollResponse implements ResponseInterface
{
    public function __construct(
        private readonly Poll $poll,
    ) {
    }

    public function getContent(): ?array
    {
        return [
            'id' => $this->poll->getId(),
            'title' => $this->poll->getTitle(),
            'options' => \array_map(
                static fn (Option $option) => $option->toArray(),
                $this->poll->getOptions(),
            ),
        ];
    }

    public function getStatusCode(): int
    {
        return 200;
    }
}
