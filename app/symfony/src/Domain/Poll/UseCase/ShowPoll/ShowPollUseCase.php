<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\ShowPoll;

use App\Domain\Poll\Exception\PollNotFoundException;
use App\Domain\Poll\Model\Poll;
use App\Domain\Poll\Repository\FindPollInterface;
use App\Domain\Shared\Response\ResponseInterface;

final class ShowPollUseCase implements ShowPollUseCaseInterface
{
    public function __construct(
        private readonly FindPollInterface $pollStore,
    ) {
    }

    public function __invoke(ShowPollRequest $showPollRequest): ResponseInterface
    {
        $showPollRequest->validate();
        $pollId = $showPollRequest->id;
        $poll = $this->pollStore->findById($pollId);

        if (!$poll instanceof Poll) {
            throw new PollNotFoundException($pollId);
        }

        return new ShowPollResponse($poll);
    }
}
