<?php

declare(strict_types=1);

namespace App\Domain\Shared\Request;

interface RequestInterface
{
    /**
     * @param array<mixed> $data
     */
    public static function createFromArray(array $data): static;

    /**
     * @return array<mixed>
     */
    public function toArray(): array;
}
