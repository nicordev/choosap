<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\Poll\UseCase\Choose\ChooseRequest;
use App\Domain\Poll\UseCase\Choose\ChooseUseCaseInterface;
use App\Infrastructure\Shared\Controller\BaseAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;

final class ChooseAction extends BaseAction
{
    #[Route(
        path: '/choices',
        name: 'choose',
        methods: [Request::METHOD_POST],
    )]
    public function __invoke(
        ChooseUseCaseInterface $choose,
        #[MapRequestPayload()]
        ChooseRequest $chooseRequest,
    ): Response {
        return $this->createJsonResponse(
            $choose($chooseRequest)
        );
    }
}
