<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\Poll\UseCase\DeletePoll\DeletePollRequest;
use App\Domain\Poll\UseCase\DeletePoll\DeletePollUseCaseInterface;
use App\Infrastructure\Shared\Controller\BaseAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

final class DeletePollAction extends BaseAction
{
    #[Route(
        path: '/polls/{id}',
        name: 'delete_poll',
        methods: [Request::METHOD_DELETE],
    )]
    public function __invoke(
        DeletePollUseCaseInterface $deletePoll,
        string $id,
    ): Response {
        $deletePollRequest = new DeletePollRequest();
        $deletePollRequest->id = $id;

        return $this->createJsonResponse(
            $deletePoll($deletePollRequest)
        );
    }
}
