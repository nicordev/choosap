<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\Poll\UseCase\EditChoice\EditChoiceRequest;
use App\Domain\Poll\UseCase\EditChoice\EditChoiceUseCaseInterface;
use App\Infrastructure\Shared\Controller\BaseAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;

final class EditChoiceAction extends BaseAction
{
    #[Route(
        path: '/choices/{id}',
        name: 'edit_choice',
        methods: [Request::METHOD_PUT],
    )]
    public function __invoke(
        EditChoiceUseCaseInterface $editChoice,
        #[MapRequestPayload()]
        EditChoiceRequest $request,
    ): Response {
        return $this->createJsonResponse(
            $editChoice($request)
        );
    }
}
