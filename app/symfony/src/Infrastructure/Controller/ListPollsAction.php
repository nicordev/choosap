<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\Poll\UseCase\ListPolls\ListPollsRequest;
use App\Domain\Poll\UseCase\ListPolls\ListPollsUseCaseInterface;
use App\Infrastructure\Shared\Controller\BaseAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

final class ListPollsAction extends BaseAction
{
    #[Route(
        path: '/polls',
        name: 'list_poll',
        methods: [Request::METHOD_GET],
    )]
    public function __invoke(
        ListPollsUseCaseInterface $listPolls,
        Request $request,
    ): Response {
        return $this->createJsonResponse(
            $listPolls(ListPollsRequest::createFromArray($request->query->all()))
        );
    }
}
