<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\Poll\UseCase\SavePoll\SavePollRequest;
use App\Domain\Poll\UseCase\SavePoll\SavePollUseCaseInterface;
use App\Infrastructure\Shared\Controller\BaseAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;

final class SavePollAction extends BaseAction
{
    #[Route(
        path: '/polls',
        name: 'save_poll',
        methods: [Request::METHOD_POST],
    )]
    public function __invoke(
        SavePollUseCaseInterface $savePoll,
        #[MapRequestPayload()]
        SavePollRequest $savePollRequest,
    ): Response {
        return $this->createJsonResponse(
            $savePoll($savePollRequest)
        );
    }
}
