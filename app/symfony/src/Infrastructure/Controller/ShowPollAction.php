<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\Poll\UseCase\ShowPoll\ShowPollRequest;
use App\Domain\Poll\UseCase\ShowPoll\ShowPollUseCaseInterface;
use App\Infrastructure\Shared\Controller\BaseAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

final class ShowPollAction extends BaseAction
{
    #[Route(
        path: '/polls/{id}',
        name: 'show_poll',
        methods: [Request::METHOD_GET],
    )]
    public function __invoke(
        ShowPollUseCaseInterface $showPoll,
        string $id,
    ): Response {
        $showPollRequest = new ShowPollRequest();
        $showPollRequest->id = $id;

        return $this->createJsonResponse(
            $showPoll($showPollRequest)
        );
    }
}
