<?php

declare(strict_types=1);

namespace App\Infrastructure\Entity;

use App\Infrastructure\Repository\ChoiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChoiceRepository::class)]
final class Choice
{
    #[ORM\Id]
    #[ORM\Column(length: 255)]
    private string $id;

    #[ORM\ManyToOne(targetEntity: Poll::class, cascade: ['remove'])]
    private Poll $poll;

    #[ORM\Column(length: 255)]
    private string $participant;

    /**
     * @var Collection<int, Option> $chosenOptions
     */
    #[ORM\ManyToMany(targetEntity: Option::class)]
    #[ORM\JoinTable(name: 'choice_chosen_option')]
    private Collection $chosenOptions;

    /**
     * @param array<Option> $chosenOptions
     */
    public function __construct(string $id, Poll $poll, string $participant, array $chosenOptions)
    {
        $this->id = $id;
        $this->poll = $poll;
        $this->participant = $participant;
        $this->chosenOptions = new ArrayCollection($chosenOptions);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPoll(): Poll
    {
        return $this->poll;
    }

    public function getParticipant(): string
    {
        return $this->participant;
    }

    /**
     * @return Collection<int, Option>
     */
    public function getChosenOptions(): Collection
    {
        return $this->chosenOptions;
    }
}
