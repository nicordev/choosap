<?php

namespace App\Infrastructure\Entity;

use App\Infrastructure\Repository\PollRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PollRepository::class)]
class Poll
{
    #[ORM\Id]
    #[ORM\Column]
    private string $id;

    #[ORM\Column(length: 255)]
    private string $title;

    /**
     * @var Collection<int, Option> $options
     */
    #[ORM\OneToMany(mappedBy: 'poll', targetEntity: Option::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $options;

    public function __construct(string $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
        $this->options = new ArrayCollection();
    }

    /**
     * @return array<string, string|array<string, string>>
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'options' => array_map(
                fn (Option $option) => $option->toArray(),
                $this->options->toArray(),
            ),
        ];
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, Option>
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(Option $option): static
    {
        if (!$this->options->contains($option)) {
            $this->options->add($option);
            $option->setPoll($this);
        }

        return $this;
    }

    public function removeOption(Option $option): static
    {
        if ($this->options->removeElement($option)) {
            // set the owning side to null (unless already changed)
            if ($option->getPoll() === $this) {
                $option->setPoll(null);
            }
        }

        return $this;
    }

    /**
     * @param array<Option> $options
     */
    public function setOptions(array $options): static
    {
        \array_walk($options, fn (Option $option) => $option->setPoll($this));
        $this->options = new ArrayCollection($options);

        return $this;
    }
}
