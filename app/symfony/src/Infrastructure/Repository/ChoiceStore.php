<?php

namespace App\Infrastructure\Repository;

use App\Domain\Poll\Model\Choice;
use App\Domain\Poll\Repository\ChooseInterface;
use App\Domain\Poll\Repository\EditChoiceInterface;
use App\Infrastructure\Entity\Choice as ChoiceEntity;

class ChoiceStore implements ChooseInterface, EditChoiceInterface
{
    public function __construct(
        private readonly PollRepository $pollRepository,
        private readonly OptionRepository $optionRepository,
        private readonly ChoiceRepository $choiceRepository,
    ) {
    }

    public function choose(Choice $choice): void
    {
        $poll = $this->pollRepository->find($choice->getPollId());
        $chosenOptions = $this->optionRepository->findBy(
            criteria: ['id' => $choice->getChosenOptionIds()]
        );
        $choiceEntity = new ChoiceEntity(
            id: $choice->getId(),
            poll: $poll,
            participant: $choice->getParticipant(),
            chosenOptions: $chosenOptions
        );
        $this->choiceRepository->save($choiceEntity);
    }

    public function editChoice(string $choiceId, array $chosenOptionIds): void
    {
        $options = $this->optionRepository->findBy(['id' => $chosenOptionIds]);
    }
}
