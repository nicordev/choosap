<?php

namespace App\Infrastructure\Repository;

use App\Infrastructure\Entity\Poll;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Poll>
 *
 * @method Poll|null find($id, $lockMode = null, $lockVersion = null)
 * @method Poll|null findOneBy(array $criteria, array $orderBy = null)
 * @method Poll[]    findAll()
 * @method Poll[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PollRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Poll::class);
    }

    public function getConnection(): Connection
    {
        return $this->_em->getConnection();
    }

    public function save(Poll $pollEntity): void
    {
        $this->_em->persist($pollEntity);
        $this->_em->flush();
    }

    public function delete(string $pollId): void
    {
        $sql = <<<SQL
            delete from poll where id = :id
        SQL;
        $deleteStatement = $this->_em->getConnection()->prepare($sql);
        $deleteStatement->bindValue('id', $pollId);
        $deleteStatement->executeStatement();
    }
}
