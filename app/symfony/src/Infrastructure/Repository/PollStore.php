<?php

namespace App\Infrastructure\Repository;

use App\Domain\Poll\Exception\PollNotFoundException;
use App\Domain\Poll\Model\Factory\PollFactory;
use App\Domain\Poll\Model\Option;
use App\Domain\Poll\Model\Poll;
use App\Domain\Poll\Repository\DeletePollInterface;
use App\Domain\Poll\Repository\FindPollInterface;
use App\Domain\Poll\Repository\SavePollInterface;
use App\Infrastructure\Entity\Option as OptionEntity;
use App\Infrastructure\Entity\Poll as PollEntity;

class PollStore implements SavePollInterface, FindPollInterface, DeletePollInterface
{
    public function __construct(
        private readonly PollRepository $pollRepository,
    ) {
    }

    public function save(Poll $poll): void
    {
        $pollEntity = new PollEntity($poll->getId(), $poll->getTitle());
        $pollEntity->setOptions(
            \array_map(
                static fn (Option $option) => new OptionEntity($option->getId(), $option->getTitle(), $option->getValue()),
                $poll->getOptions(),
            ),
        );
        $this->pollRepository->save($pollEntity);
    }

    public function findById(string $id): ?Poll
    {
        $pollEntity = $this->pollRepository->createQueryBuilder('poll')
            ->addSelect('options')
            ->join('poll.options', 'options')
            ->where('poll.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if (!$pollEntity instanceof PollEntity) {
            throw new PollNotFoundException($id);
        }

        return PollFactory::createFromArray($pollEntity->toArray());
    }

    /**
     * @return array<Poll>
     */
    public function find(int $offset, int $limit): array
    {
        $polls = $this->pollRepository->createQueryBuilder('poll')
            ->addSelect('options')
            ->join('poll.options', 'options')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getArrayResult()
        ;

        return array_map(
            static fn (array $pollData) => PollFactory::createFromArray($pollData),
            $polls,
        );
    }

    public function delete(string $pollId): void
    {
        $this->pollRepository->delete($pollId);
    }
}
