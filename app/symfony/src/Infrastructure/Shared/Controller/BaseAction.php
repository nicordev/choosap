<?php

declare(strict_types=1);

namespace App\Infrastructure\Shared\Controller;

use App\Domain\Shared\Response\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseAction extends AbstractController
{
    public function createJsonResponse(ResponseInterface $response): Response
    {
        return new JsonResponse(
            data: $response->getContent(),
            status: $response->getStatusCode(),
        );
    }
}
