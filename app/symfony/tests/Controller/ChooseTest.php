<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\TestHelper\FixturesTrait;
use App\Tests\TestHelper\ProvidePollsTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class ChooseTest extends WebTestCase
{
    use FixturesTrait;
    use ProvidePollsTrait;

    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->clearDatabase();
    }

    /**
     * @dataProvider provide3Polls
     *
     * @param array<int, array{'id'?: string, 'title': string, 'options': array<int, array{'id'?: string, 'title': string, 'value': string}>}> $polls
     */
    public function testCanChoose(array $polls): void
    {
        $this->loadPolls($polls);

        $this->client->request(
            method: 'POST',
            uri: '/choices',
            content: json_encode([
                'pollId' => 'dummypoll1',
                'participant' => 'sarah croche',
                'chosenOptionIds' => ['dummypoll1dummyoption2', 'dummypoll1dummyoption3'],
            ]),
            server: [
                'CONTENT_TYPE' => 'application/json',
            ]
        );

        self::assertResponseIsSuccessful();
        self::assertResponseStatusCodeSame(200);

        $content = \json_decode(
            json: $this->client->getResponse()->getContent(),
            associative: true,
            flags: \JSON_THROW_ON_ERROR
        );
        self::assertArrayHasKey('id', $content);
    }
}
