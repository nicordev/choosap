<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\TestHelper\FixturesTrait;
use App\Tests\TestHelper\ProvidePollsTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class DeletePollTest extends WebTestCase
{
    use ProvidePollsTrait;
    use FixturesTrait;

    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->clearDatabase();
    }

    /**
     * @dataProvider provide3Polls
     *
     * @param array<int, array{'id'?: string, 'title': string, 'options': array<int, array{'id'?: string, 'title': string, 'value': string}>}> $polls
     */
    public function testCanDeleteAPoll(array $polls): void
    {
        $pollId = 'dummypoll2';
        $this->loadPolls($polls);

        $this->client->request(
            method: 'DELETE',
            uri: '/polls/'.$pollId,
        );

        self::assertResponseStatusCodeSame(204);
        $this->assertPollIsDeleted($pollId);
    }
}
