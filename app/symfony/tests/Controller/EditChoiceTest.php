<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\TestHelper\FixturesTrait;
use App\Tests\TestHelper\ProvidePollsTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class EditChoiceTest extends WebTestCase
{
    use FixturesTrait;
    use ProvidePollsTrait;

    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->clearDatabase();
    }

    /**
     * @dataProvider provide1Poll3Choices
     *
     * @param array{'id': string, 'title': string, 'options': array<int, array{'id': string, 'title': string, 'value': string}>} $poll
     * @param array<int, array{'id': string, 'pollId': string, 'participant': string, 'chosenOptionIds': array<string>}>         $choices
     */
    public function testCanEditChoice(array $poll, array $choices): void
    {
        $this->loadPolls([$poll]);
        $this->loadChoices($choices);
        $pollId = $poll['id'];
        $chosenOptionIds = $choices[1]['chosenOptionIds'];

        $this->client->request(
            method: 'PUT',
            uri: "/choices/{$pollId}",
            content: json_encode([
                'choiceId' => $pollId,
                'chosenOptionIds' => $chosenOptionIds,
            ]),
            server: [
                'CONTENT_TYPE' => 'application/json',
            ]
        );

        self::assertResponseIsSuccessful();
        self::assertResponseStatusCodeSame(204);

        $content = $this->client->getResponse()->getContent();
        self::assertSame('', $content);
    }
}
