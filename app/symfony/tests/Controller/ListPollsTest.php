<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\TestHelper\FixturesTrait;
use App\Tests\TestHelper\ProvidePollsTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class ListPollsTest extends WebTestCase
{
    use ProvidePollsTrait;
    use FixturesTrait;

    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->clearDatabase();
    }

    /**
     * @dataProvider provide3Polls
     *
     * @param array<int, array{'id'?: string, 'title': string, 'options': array<int, array{'id'?: string, 'title': string, 'value': string}>}> $polls
     */
    public function testCanListPolls(array $polls): void
    {
        $this->loadPolls($polls);
        $this->client->request(
            method: 'GET',
            uri: '/polls',
        );

        self::assertResponseIsSuccessful();
        self::assertResponseStatusCodeSame(200);

        $content = \json_decode(
            json: $this->client->getResponse()->getContent(),
            associative: true,
            flags: \JSON_THROW_ON_ERROR
        );
        $this->thenResponseContainsPolls($content, $polls);
    }

    /**
     * @param array<mixed>                                                                                                                     $content
     * @param array<int, array{'id'?: string, 'title': string, 'options': array<int, array{'id'?: string, 'title': string, 'value': string}>}> $expectedPolls
     */
    private function thenResponseContainsPolls(array $content, array $expectedPolls): void
    {
        self::assertCount(count($expectedPolls), $content);

        for ($i = 0, $pollsCount = count($expectedPolls); $i < $pollsCount; ++$i) {
            $expectedPoll = $expectedPolls[$i];
            $actualPoll = $content[$i];
            $expectedPollId = $expectedPoll['id'] ?? 'dummypoll'.($i + 1);
            self::assertSame($expectedPollId, $actualPoll['id']);
            self::assertSame($expectedPoll['title'], $actualPoll['title']);

            for ($j = 0, $optionsCount = count($expectedPoll['options']); $j < $optionsCount; ++$j) {
                $expectedOption = $expectedPoll['options'][$j];
                $actualOption = $actualPoll['options'][$j];
                $expectedOptionId = $expectedOption['id'] ?? $expectedPollId.'dummyoption'.($j + 1);
                self::assertSame($expectedOptionId, $actualOption['id']);
                self::assertSame($expectedOption['title'], $actualOption['title']);
                self::assertSame($expectedOption['value'], $actualOption['value']);
            }
        }
    }
}
