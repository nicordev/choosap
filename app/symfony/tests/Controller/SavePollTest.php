<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\TestHelper\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class SavePollTest extends WebTestCase
{
    use FixturesTrait;

    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->clearDatabase();
    }

    public function testCanSavePoll(): void
    {
        $this->client->request(
            method: 'POST',
            uri: '/polls',
            content: json_encode([
                'title' => 'dummy poll title',
                'options' => [
                    [
                        'title' => 'dummy option 1 title',
                        'value' => 'dummy option 1 value',
                    ],
                    [
                        'title' => 'dummy option 2 title',
                        'value' => 'dummy option 2 value',
                    ],
                ],
            ]),
            server: [
                'CONTENT_TYPE' => 'application/json',
            ]
        );

        self::assertResponseIsSuccessful();
    }
}
