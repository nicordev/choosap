<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\TestHelper\FixturesTrait;
use App\Tests\TestHelper\ProvidePollsTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class ShowPollTest extends WebTestCase
{
    use ProvidePollsTrait;
    use FixturesTrait;

    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->clearDatabase();
    }

    /**
     * @dataProvider provide3Polls
     *
     * @param array<int, array{'id'?: string, 'title': string, 'options': array<int, array{'id'?: string, 'title': string, 'value': string}>}> $polls
     */
    public function testCanShowPoll(array $polls): void
    {
        $pollId = 'dummypoll2';
        $this->loadPolls($polls);
        $this->client->request(
            method: 'GET',
            uri: '/polls/'.$pollId,
        );

        self::assertResponseStatusCodeSame(200);

        $content = \json_decode(
            json: $this->client->getResponse()->getContent(),
            associative: true,
            flags: \JSON_THROW_ON_ERROR
        );
        $this->thenResponseShowsAPoll($content, [...$polls[1], 'id' => $pollId]);
    }

    /**
     * @param array{'id'?: string, 'title': string, 'options': array<int, array{'id'?: string, 'title': string, 'value': string}>} $actualPoll
     * @param array{'id'?: string, 'title': string, 'options': array<int, array{'id'?: string, 'title': string, 'value': string}>} $expectedPoll
     */
    private function thenResponseShowsAPoll(array $actualPoll, array $expectedPoll): void
    {
        $this->assertPollArrayHasKeys($actualPoll, $expectedPoll);
        self::assertSame($expectedPoll['id'], $actualPoll['id']);
        self::assertSame($expectedPoll['title'], $actualPoll['title']);

        for ($i = 0, $optionsCount = count($expectedPoll['options']); $i < $optionsCount; ++$i) {
            $expectedOption = $expectedPoll['options'][$i];
            $actualOption = $actualPoll['options'][$i];
            $expectedOptionId = $expectedOption['id'] ?? $expectedPoll['id'].'dummyoption'.($i + 1);
            self::assertSame($expectedOptionId, $actualOption['id']);
            self::assertSame($expectedOption['title'], $actualOption['title']);
            self::assertSame($expectedOption['value'], $actualOption['value']);
        }
    }

    /**
     * @param array{'id': string, 'title': string, 'options': array<int, array{'id': string, 'title': string, 'value': string}>} $poll
     */
    private function assertPollArrayHasKeys(array ...$poll): void
    {
        foreach ($poll as $currentPoll) {
            self::assertArrayHasKey('id', $currentPoll);
            self::assertArrayHasKey('title', $currentPoll);
        }
    }
}
