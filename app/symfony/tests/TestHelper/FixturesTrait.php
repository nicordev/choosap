<?php

declare(strict_types=1);

namespace App\Tests\TestHelper;

use Doctrine\DBAL\Connection;

trait FixturesTrait
{
    private ?Connection $connection = null;

    private function initConnection(): void
    {
        if (null === $this->connection) {
            $this->connection = static::getContainer()->get('doctrine')->getConnection();
        }
    }

    private function clearDatabase(): void
    {
        $this->initConnection();

        $this->connection->executeStatement('delete from option');
        $this->connection->executeStatement('delete from poll');
        $this->connection->executeStatement('delete from choice');
        $this->connection->executeStatement('delete from choice_chosen_option');
    }

    /**
     * If no id, generate one like dummypoll1dummyoption1.
     *
     * @param array<array<mixed>> $polls
     */
    public function loadPolls(array $polls): void
    {
        $this->initConnection();

        try {
            $this->connection->beginTransaction();
            $i = 0;
            foreach ($polls as $poll) {
                ++$i;
                $pollId = $poll['id'] ?? "dummypoll$i";
                $pollTitle = $poll['title'] ?? "dummy poll $i title";
                $sql = <<<SQL
                    insert into poll(id, title) values ("{$pollId}", "{$pollTitle}")
                SQL;
                $this->connection->executeStatement($sql);

                $j = 0;
                foreach ($poll['options'] as $option) {
                    ++$j;
                    $optionId = $option['id'] ?? $pollId."dummyoption$j";
                    $optionTitle = $option['title'] ?? "dummy option $j title";
                    $optionValue = $option['value'] ?? "dummy option $j value";
                    $sql = <<<SQL
                        insert into option(id, title, "value", poll_id) values ("{$optionId}", "{$optionTitle}", "{$optionValue}", "{$pollId}")
                    SQL;
                    $this->connection->executeStatement($sql);
                }
            }
            $this->connection->commit();
        } catch (\Throwable $e) {
            $this->connection->rollBack();
            throw $e;
        }
    }

    /**
     * @param array<array<mixed>> $choices
     */
    public function loadChoices(array $choices): void
    {
        $this->initConnection();

        try {
            $this->connection->beginTransaction();
            foreach ($choices as $choice) {
                $sql = <<<SQL
                    insert into choice(id, poll_id, participant) values ("{$choice['id']}", "{$choice['pollId']}", "{$choice['participant']}")
                SQL;
                $this->connection->executeStatement($sql);

                $this->chooseOptions($choice['id'], $choice['chosenOptionIds']);
            }
            $this->connection->commit();
        } catch (\Throwable $e) {
            $this->connection->rollBack();
            throw $e;
        }
    }

    /**
     * @param array<string> $optionIds
     */
    private function chooseOptions(string $choiceId, array $optionIds): void
    {
        $values = \implode(
            ",\n",
            array_map(static fn ($optionId) => "('{$choiceId}', '{$optionId}')", $optionIds)
        );
        $sql = <<<SQL
            insert into choice_chosen_option(choice_id, option_id)
            values {$values}
        SQL;
        $this->connection->executeStatement($sql);
    }

    public function assertPollIsDeleted(string $pollId): void
    {
        $sql = <<<SQL
            select 1 from poll where id = :id
        SQL;
        $query = $this->connection->prepare($sql);
        $query->bindValue('id', $pollId);
        $result = $query->executeQuery()->fetchOne();
        self::assertFalse($result);
    }
}
