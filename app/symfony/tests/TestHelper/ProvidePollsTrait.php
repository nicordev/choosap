<?php

declare(strict_types=1);

namespace App\Tests\TestHelper;

trait ProvidePollsTrait
{
    public function provide3Polls(): \Generator
    {
        yield '3 valid polls' => [
                [
                    [
                        'title' => 'dummy poll 1 title',
                        'options' => [
                            [
                                'title' => 'dummy option 11 title',
                                'value' => 'dummy option 11 value',
                            ],
                            [
                                'title' => 'dummy option 12 title',
                                'value' => 'dummy option 12 value',
                            ],
                        ],
                    ],
                    [
                        'title' => 'dummy poll 2 title',
                        'options' => [
                            [
                                'title' => 'dummy option 21 title',
                                'value' => 'dummy option 21 value',
                            ],
                            [
                                'title' => 'dummy option 22 title',
                                'value' => 'dummy option 22 value',
                            ],
                        ],
                    ],
                    [
                        'title' => 'dummy poll 3 title',
                        'options' => [
                            [
                                'title' => 'dummy option 31 title',
                                'value' => 'dummy option 31 value',
                            ],
                            [
                                'title' => 'dummy option 32 title',
                                'value' => 'dummy option 32 value',
                            ],
                        ],
                    ],
                ],
            ]
        ;
    }

    public function provide1Poll3Choices(): \Generator
    {
        yield '1 poll 3 choices' => [
            [
                'id' => 'dummypoll1',
                'title' => 'dummy poll 1 title',
                'options' => [
                    [
                        'id' => 'dummyoption11',
                        'pollId' => 'dummypoll 1',
                        'title' => 'dummy option 11 title',
                        'value' => 'dummy option 11 value',
                    ],
                    [
                        'id' => 'dummyoption12',
                        'pollId' => 'dummypoll1',
                        'title' => 'dummy option 12 title',
                        'value' => 'dummy option 12 value',
                    ],
                    [
                        'id' => 'dummyoption13',
                        'pollId' => 'dummypoll1',
                        'title' => 'dummy option 13 title',
                        'value' => 'dummy option 13 value',
                    ],
                ],
            ],
            [
                [
                    'id' => 'dummychoice1',
                    'pollId' => 'dummypoll1',
                    'participant' => 'sarah croche',
                    'chosenOptionIds' => [
                        'dummyoption11',
                        'dummyoption12',
                    ],
                ],
                [
                    'id' => 'dummychoice2',
                    'pollId' => 'dummypoll1',
                    'participant' => 'lara clette',
                    'chosenOptionIds' => [
                        'dummyoption13',
                    ],
                ],
            ],
        ];
    }
}
