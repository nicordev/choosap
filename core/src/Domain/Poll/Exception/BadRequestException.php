<?php

declare(strict_types=1);

namespace App\Domain\Poll\Exception;

final class BadRequestException extends \RuntimeException
{
    /**
     * @param array<string> $violations
     */
    public function __construct(array $violations)
    {
        parent::__construct(
            \sprintf('Bad Request: %s', implode(', ', $violations)),
            400,
        );
    }
}
