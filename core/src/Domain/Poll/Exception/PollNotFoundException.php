<?php

declare(strict_types=1);

namespace App\Domain\Poll\Exception;

final class PollNotFoundException extends \RuntimeException
{
    public function __construct(string $pollId)
    {
        parent::__construct(
            \sprintf('Poll with id %s not found.', $pollId),
            404,
        );
    }
}
