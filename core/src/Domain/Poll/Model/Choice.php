<?php

declare(strict_types=1);

namespace App\Domain\Poll\Model;

final class Choice
{
    /**
     * @param array<string> $chosenOptionIds
     */
    public function __construct(
        private readonly string $id,
        private readonly string $pollId,
        private readonly string $participant,
        private array $chosenOptionIds,
    ) {
    }

    /**
     * @param array<string> $chosenOptionIds
     */
    public function edit(array $chosenOptionIds): void
    {
        $this->chosenOptionIds = $chosenOptionIds;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPollId(): string
    {
        return $this->pollId;
    }

    public function getParticipant(): string
    {
        return $this->participant;
    }

    /**
     * @return array<string>
     */
    public function getChosenOptionIds(): array
    {
        return $this->chosenOptionIds;
    }
}
