<?php

declare(strict_types=1);

namespace App\Domain\Poll\Model\Factory;

interface IdFactoryInterface
{
    /**
     * @param array<mixed> $data
     */
    public function create(array $data = []): string;
}
