<?php

declare(strict_types=1);

namespace App\Domain\Poll\Model\Factory;

use App\Domain\Poll\Model\Poll;

final class PollFactory
{
    /**
     * @param array<string, string|array<string>> $data
     */
    public static function createFromArray(array $data): Poll
    {
        return new Poll(
            $data['id'],
            $data['title'],
            array_map(
                [OptionFactory::class, 'createFromArray'],
                $data['options'],
            ),
        );
    }
}
