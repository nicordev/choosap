<?php

declare(strict_types=1);

namespace App\Domain\Poll\Model;

final class Poll
{
    public function __construct(
        private string $id,
        private string $title,
        /**
         * @var array<Option>
         */
        private array $options,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return array<Option>
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @return array<string, string|array<string>>
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'options' => array_map(
                static fn (Option $option) => $option->toArray(),
                $this->getOptions(),
            ),
        ];
    }
}
