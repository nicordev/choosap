<?php

declare(strict_types=1);

namespace App\Domain\Poll\Repository;

use App\Domain\Poll\Model\Poll;

interface FindPollInterface
{
    public function findById(string $id): ?Poll;

    /**
     * @return array<Poll>
     */
    public function find(int $offset, int $limit): array;
}
