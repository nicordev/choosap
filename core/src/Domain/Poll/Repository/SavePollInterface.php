<?php

declare(strict_types=1);

namespace App\Domain\Poll\Repository;

use App\Domain\Poll\Model\Poll;

interface SavePollInterface
{
    public function save(Poll $poll): void;
}
