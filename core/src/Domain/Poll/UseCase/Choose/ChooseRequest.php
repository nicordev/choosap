<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\Choose;

use App\Domain\Poll\Exception\BadRequestException;
use App\Domain\Shared\Request\BaseRequest;

final class ChooseRequest extends BaseRequest
{
    public string $pollId;
    public string $participant;
    /**
     * @var array<string>
     */
    public array $chosenOptionIds;

    public function validate(): void
    {
        parent::validate();

        foreach ($this->chosenOptionIds as $id) {
            if (!\is_string($id)) {
                throw new BadRequestException(['chosenOptionIds must contain only strings']);
            }
        }
    }
}
