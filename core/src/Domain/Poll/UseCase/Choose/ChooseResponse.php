<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\Choose;

use App\Domain\Shared\Response\ResponseInterface;

final class ChooseResponse implements ResponseInterface
{
    public function __construct(
        private readonly string $id
    ) {
    }

    public function getContent(): ?array
    {
        return ['id' => $this->id];
    }

    public function getStatusCode(): int
    {
        return 200;
    }
}
