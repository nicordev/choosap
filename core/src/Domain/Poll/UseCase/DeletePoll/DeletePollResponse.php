<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\DeletePoll;

use App\Domain\Shared\Response\ResponseInterface;

final class DeletePollResponse implements ResponseInterface
{
    public function getContent(): ?array
    {
        return null;
    }

    public function getStatusCode(): int
    {
        return 204;
    }
}
