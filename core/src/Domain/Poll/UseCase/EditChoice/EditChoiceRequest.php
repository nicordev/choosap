<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\EditChoice;

use App\Domain\Shared\Request\BaseRequest;

final class EditChoiceRequest extends BaseRequest
{
    public string $choiceId;
    /**
     * @var array<string>
     */
    public array $chosenOptionIds;
}
