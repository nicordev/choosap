<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\EditChoice;

use App\Domain\Shared\Response\ResponseInterface;

final class EditChoiceResponse implements ResponseInterface
{
    public function getContent(): ?array
    {
        return null;
    }

    public function getStatusCode(): int
    {
        return 204;
    }
}
