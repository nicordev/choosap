<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\EditChoice;

use App\Domain\Shared\Response\ResponseInterface;

interface EditChoiceUseCaseInterface
{
    public function __invoke(EditChoiceRequest $editChoiceRequest): ResponseInterface;
}
