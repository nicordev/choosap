<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\ListPolls;

use App\Domain\Poll\Model\Poll;
use App\Domain\Shared\Response\ResponseInterface;

final class ListPollsResponse implements ResponseInterface
{
    /**
     * @param array<Poll> $polls
     */
    public function __construct(
        private readonly array $polls,
    ) {
    }

    /**
     * @return array<array<string, int|string|array<string>>>|null
     */
    public function getContent(): ?array
    {
        return array_map(
            static fn (Poll $poll) => $poll->toArray(),
            $this->polls,
        );
    }

    public function getStatusCode(): int
    {
        return 200;
    }
}
