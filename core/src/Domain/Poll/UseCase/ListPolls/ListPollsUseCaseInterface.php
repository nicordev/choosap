<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\ListPolls;

use App\Domain\Shared\Response\ResponseInterface;

interface ListPollsUseCaseInterface
{
    public function __invoke(ListPollsRequest $listPollsRequest): ResponseInterface;
}
