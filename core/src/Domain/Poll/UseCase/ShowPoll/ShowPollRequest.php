<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\ShowPoll;

use App\Domain\Shared\Request\BaseRequest;

final class ShowPollRequest extends BaseRequest
{
    public string $id;
}
