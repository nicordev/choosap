<?php

declare(strict_types=1);

namespace App\Domain\Poll\UseCase\ShowPoll;

use App\Domain\Shared\Response\ResponseInterface;

interface ShowPollUseCaseInterface
{
    public function __invoke(ShowPollRequest $showPollRequest): ResponseInterface;
}
