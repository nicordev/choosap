<?php

declare(strict_types=1);

namespace App\Domain\Shared\Request;

use App\Domain\Poll\Exception\BadRequestException;

abstract class BaseRequest implements RequestInterface
{
    final public function __construct()
    {
    }

    public static function createFromArray(array $data): static
    {
        $instance = new static();

        foreach ($data as $property => $value) {
            if (!\property_exists(static::class, $property)) {
                throw new \LogicException("$property does not exists for class ".static::class);
            }

            $instance->$property = $value;
        }

        return $instance;
    }

    public function toArray(): array
    {
        return \get_object_vars($this);
    }

    public function validate(): void
    {
        $missingProperties = $this->getMissingProperties();

        if ([] !== $missingProperties) {
            throw new BadRequestException(array_map(static fn (string $missingProperty): string => "missing {$missingProperty}", $missingProperties));
        }
    }

    /**
     * @return array<string>
     */
    private function getMissingProperties(): array
    {
        $requestProperties = (new \ReflectionClass($this))->getProperties(\ReflectionProperty::IS_PUBLIC);
        $actualProperties = \get_object_vars($this);

        return \array_diff(
            array_map(
                static fn (\ReflectionProperty $property): string => $property->getName(),
                $requestProperties,
            ),
            \array_keys($actualProperties),
        );
    }
}
