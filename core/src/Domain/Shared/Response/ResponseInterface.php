<?php

declare(strict_types=1);

namespace App\Domain\Shared\Response;

interface ResponseInterface
{
    /**
     * @return array<mixed>|null
     */
    public function getContent(): ?array;

    public function getStatusCode(): int;
}
