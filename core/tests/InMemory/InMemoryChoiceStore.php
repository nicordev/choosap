<?php

declare(strict_types=1);

namespace App\Tests\InMemory;

use App\Domain\Poll\Model\Choice;
use App\Domain\Poll\Repository\ChooseInterface;
use App\Domain\Poll\Repository\EditChoiceInterface;

final class InMemoryChoiceStore implements ChooseInterface, EditChoiceInterface
{
    public function __construct(
        /**
         * @var array<Choice>
         */
        private array $choices,
    ) {
        foreach ($choices as $choice) {
            $this->choices[$choice->getId()] = $choice;
        }
    }

    public function findById(string $id): ?Choice
    {
        if (!$this->choiceExists($id)) {
            return null;
        }

        return $this->choices[$id];
    }

    /**
     * @return array<Choice>
     */
    public function find(int $offset, int $limit): array
    {
        $choices = [];
        $index = -1;
        $count = 0;

        foreach ($this->choices as $choice) {
            ++$index;

            if ($index < $offset) {
                continue;
            }

            if ($count > $limit) {
                break;
            }

            $choices[] = $choice;
            ++$count;
        }

        return $choices;
    }

    public function choiceExists(string $id): bool
    {
        return \array_key_exists($id, $this->choices);
    }

    public function choose(Choice $choice): void
    {
        $this->choices[$choice->getId()] = $choice;
    }

    /**
     * @return array<mixed>
     */
    public function getChoices(string $pollId): array
    {
        return array_filter($this->choices, fn (Choice $choice) => $choice->getPollId() === $pollId);
    }

    public function editChoice(string $choiceId, array $chosenOptionIds): void
    {
        $this->choices[$choiceId]->edit($chosenOptionIds);
    }
}
