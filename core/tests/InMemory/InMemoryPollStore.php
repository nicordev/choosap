<?php

declare(strict_types=1);

namespace App\Tests\InMemory;

use App\Domain\Poll\Model\Poll;
use App\Domain\Poll\Repository\DeletePollInterface;
use App\Domain\Poll\Repository\FindPollInterface;
use App\Domain\Poll\Repository\SavePollInterface;

final class InMemoryPollStore implements SavePollInterface, FindPollInterface, DeletePollInterface
{
    public function __construct(
        /**
         * @var array<Poll>
         */
        private array $polls,
    ) {
    }

    public function save(Poll $poll): void
    {
        $this->polls[$poll->getId()] = $poll;
    }

    public function findById(string $id): ?Poll
    {
        if (!$this->pollExists($id)) {
            return null;
        }

        return $this->polls[$id];
    }

    /**
     * @return array<Poll>
     */
    public function find(int $offset, int $limit): array
    {
        $polls = [];
        $index = -1;
        $count = 0;

        foreach ($this->polls as $poll) {
            ++$index;

            if ($index < $offset) {
                continue;
            }

            if ($count > $limit) {
                break;
            }

            $polls[] = $poll;
            ++$count;
        }

        return $polls;
    }

    public function delete(string $id): void
    {
        $this->assertPollExists($id);

        unset($this->polls[$id]);
    }

    public function pollExists(string $id): bool
    {
        return \array_key_exists($id, $this->polls);
    }

    public function pollWithTitleExists(string $title): bool
    {
        return [] !== \array_filter($this->polls, static fn (Poll $poll) => $poll->getTitle() === $title);
    }

    private function assertPollExists(string $id): void
    {
        if (!$this->pollExists($id)) {
            throw new \LogicException("FROM MOCK: Poll $id not found.");
        }
    }
}
