<?php

declare(strict_types=1);

namespace App\Tests\Mock;

use App\Domain\Poll\Model\Factory\IdFactoryInterface;

final class IdFactoryMock implements IdFactoryInterface
{
    private int $index = 0;

    public function __construct(
        /**
         * @var array<string>
         */
        private array $ids,
    ) {
    }

    public function setIndex(int $index): void
    {
        $idsCount = count($this->ids);

        if ($index >= $idsCount) {
            $this->index = $idsCount;

            return;
        }

        if ($index < 0) {
            $this->index = 0;

            return;
        }

        $this->index = $index;
    }

    public function getCurrentId(): string
    {
        return $this->ids[$this->index];
    }

    public function create(array $data = []): string
    {
        return $this->ids[$this->index];
    }
}
