<?php

/*
|--------------------------------------------------------------------------
| Test Case
|--------------------------------------------------------------------------
|
| The closure you provide to your test functions is always bound to a specific PHPUnit test
| case class. By default, that class is "PHPUnit\Framework\TestCase". Of course, you may
| need to change it using the "uses()" function to bind a different classes or traits.
|
*/

// uses(Tests\TestCase::class)->in('Feature');

/*
|--------------------------------------------------------------------------
| Expectations
|--------------------------------------------------------------------------
|
| When you're writing tests, you often need to check that values meet certain conditions. The
| "expect()" function gives you access to a set of "expectations" methods that you can use
| to assert different things. Of course, you may extend the Expectation API at any time.
|
*/

use App\Domain\Poll\Model\Factory\ChoiceFactory;
use App\Domain\Poll\Model\Factory\PollFactory;
use App\Domain\Shared\Response\ResponseInterface;
use App\Tests\InMemory\InMemoryChoiceStore;
use App\Tests\InMemory\InMemoryPollStore;

expect()->extend('toBeOne', function () {
    return $this->toBe(1);
});

/*
|--------------------------------------------------------------------------
| Functions
|--------------------------------------------------------------------------
|
| While Pest is very powerful out-of-the-box, you may have some testing code specific to your
| project that you don't want to repeat in every file. Here you can also expose helpers as
| global functions to help you to reduce the number of lines of code in your test files.
|
*/

function given_there_are_polls_in_database(array $pollsData): InMemoryPollStore
{
    foreach ($pollsData as $pollData) {
        $polls[$pollData['id']] = PollFactory::createFromArray($pollData);
    }

    return new InMemoryPollStore($polls);
}

function given_there_are_choices_in_database(array $choicesData): InMemoryChoiceStore
{
    foreach ($choicesData as $choiceData) {
        $choices[$choiceData['id']] = ChoiceFactory::createFromArray($choiceData);
    }

    return new InMemoryChoiceStore($choices);
}

function expectResponseToBe(ResponseInterface $response, int $statusCode, ?array $content): void
{
    expect($response->getStatusCode())->toBe($statusCode);
    expect($response->getContent())->toBe($content);
    expect(get_class($response))->toImplement(ResponseInterface::class);
}
