<?php

use App\Domain\Poll\Exception\BadRequestException;
use App\Domain\Shared\Request\BaseRequest;

test('can validate', function () {
    // given
    $request = new class() extends BaseRequest {
        public string $mandatoryProperty;
        public string $optionalProperty = '';
    };

    // when
    $exception = null;
    try {
        $request->validate();
    } catch (BadRequestException $exception) {
    }

    // then
    expect($exception)->not()->toBeNull('BadRequestException not thrown.');
    expect($exception->getMessage())->toBe('Bad Request: missing mandatoryProperty');
});
