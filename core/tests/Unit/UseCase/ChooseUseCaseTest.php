<?php

declare(strict_types=1);

use App\Domain\Poll\Exception\BadRequestException;
use App\Domain\Poll\UseCase\Choose\ChooseRequest;
use App\Domain\Poll\UseCase\Choose\ChooseUseCase;
use App\Tests\InMemory\InMemoryChoiceStore;
use App\Tests\Mock\IdFactoryMock;

test('can choose', function () {
    // given
    given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'dummy option id 11',
                    'value' => 'dummy value 11',
                    'title' => 'dummy title 11',
                ],
                [
                    'id' => 'dummy option id 12',
                    'value' => 'dummy value 12',
                    'title' => 'dummy title 12',
                ],
                [
                    'id' => 'dummy option id 13',
                    'value' => 'dummy value 13',
                    'title' => 'dummy title 13',
                ],
            ],
        ],
    ]);
    $choiceStore = new InMemoryChoiceStore([]);
    $idFactory = new IdFactoryMock(['dummy id 1']);
    $chooseUseCase = new ChooseUseCase($choiceStore, $idFactory);
    $chooseRequest = new ChooseRequest();
    $chooseRequest->pollId = 'dummy poll id 1';
    $chooseRequest->chosenOptionIds = ['dummy option id 12', 'dummy option id 13'];
    $chooseRequest->participant = 'dummy participant';

    // when
    $response = $chooseUseCase($chooseRequest);

    // then
    expectResponseToBe(
        response: $response,
        statusCode: 200,
        content: [
            'id' => $idFactory->getCurrentId(),
        ],
    );
    expect($choiceStore->choiceExists($idFactory->getCurrentId()))->toBeTrue();
});

test('cannot choose with missing values', function () {
    // given
    given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'dummy option id 11',
                    'value' => 'dummy value 11',
                    'title' => 'dummy title 11',
                ],
                [
                    'id' => 'dummy option id 12',
                    'value' => 'dummy value 12',
                    'title' => 'dummy title 12',
                ],
                [
                    'id' => 'dummy option id 13',
                    'value' => 'dummy value 13',
                    'title' => 'dummy title 13',
                ],
            ],
        ],
    ]);
    $choiceStore = new InMemoryChoiceStore([]);
    $idFactory = new IdFactoryMock(['dummy id 1']);
    $chooseUseCase = new ChooseUseCase($choiceStore, $idFactory);
    $chooseRequest = new ChooseRequest();

    // when
    $exception = null;
    try {
        $chooseUseCase($chooseRequest);
    } catch (BadRequestException $exception) {
    }

    // then
    expect($exception)->not()->toBeNull('BadRequestException not thrown.');
    expect($exception->getMessage())->toBe('Bad Request: missing pollId, missing participant, missing chosenOptionIds');
    expect($choiceStore->choiceExists($idFactory->getCurrentId()))->toBeFalse();
});

test('cannot choose with wrong values', function () {
    // given
    given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'dummy option id 11',
                    'value' => 'dummy value 11',
                    'title' => 'dummy title 11',
                ],
                [
                    'id' => 'dummy option id 12',
                    'value' => 'dummy value 12',
                    'title' => 'dummy title 12',
                ],
                [
                    'id' => 'dummy option id 13',
                    'value' => 'dummy value 13',
                    'title' => 'dummy title 13',
                ],
            ],
        ],
    ]);
    $choiceStore = new InMemoryChoiceStore([]);
    $idFactory = new IdFactoryMock(['dummy id 1']);
    $chooseUseCase = new ChooseUseCase($choiceStore, $idFactory);
    $chooseRequest = new ChooseRequest();
    $chooseRequest->pollId = 'dummy poll id 1';
    $chooseRequest->chosenOptionIds = [1, 2];
    $chooseRequest->participant = 'dummy participant';

    // when
    $exception = null;
    try {
        $chooseUseCase($chooseRequest);
    } catch (BadRequestException $exception) {
    }

    // then
    expect($exception)->not()->toBeNull('BadRequestException not thrown.');
    expect($exception->getMessage())->toBe('Bad Request: chosenOptionIds must contain only strings');
    expect($choiceStore->choiceExists($idFactory->getCurrentId()))->toBeFalse();
});
