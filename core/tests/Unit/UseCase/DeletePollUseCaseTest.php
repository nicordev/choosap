<?php

declare(strict_types=1);

use App\Domain\Poll\Exception\BadRequestException;
use App\Domain\Poll\UseCase\DeletePoll\DeletePollRequest;
use App\Domain\Poll\UseCase\DeletePoll\DeletePollUseCase;

test('can delete a poll', function () {
    // given
    $pollStore = given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'option 11',
                    'value' => 'dummy option value 11',
                    'title' => 'dummy option title 11',
                ],
                [
                    'id' => 'option 12',
                    'value' => 'dummy option value 12',
                    'title' => 'dummy option title 12',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 2',
            'title' => 'dummy poll title 2',
            'options' => [
                [
                    'id' => 'option 21',
                    'value' => 'dummy option value 21',
                    'title' => 'dummy option title 21',
                ],
                [
                    'id' => 'option 22',
                    'value' => 'dummy option value 22',
                    'title' => 'dummy option title 22',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 3',
            'title' => 'dummy poll title 3',
            'options' => [
                [
                    'id' => 'option 31',
                    'value' => 'dummy option value 31',
                    'title' => 'dummy option title 31',
                ],
                [
                    'id' => 'option 32',
                    'value' => 'dummy option value 32',
                    'title' => 'dummy option title 32',
                ],
            ],
        ],
    ]);
    $deletePollRequest = new DeletePollRequest();
    $deletePollRequest->id = 'dummy poll id 2';
    $deletePollUseCase = new DeletePollUseCase($pollStore);

    // when
    $response = $deletePollUseCase($deletePollRequest);

    // then
    expectResponseToBe(
        response: $response,
        statusCode: 204,
        content: null,
    );
    expect($pollStore->pollExists($deletePollRequest->id))->toBeFalse();
});

test('cannot delete a poll with missing values', function () {
    // given
    $pollStore = given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'option 11',
                    'value' => 'dummy option value 11',
                    'title' => 'dummy option title 11',
                ],
                [
                    'id' => 'option 12',
                    'value' => 'dummy option value 12',
                    'title' => 'dummy option title 12',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 2',
            'title' => 'dummy poll title 2',
            'options' => [
                [
                    'id' => 'option 21',
                    'value' => 'dummy option value 21',
                    'title' => 'dummy option title 21',
                ],
                [
                    'id' => 'option 22',
                    'value' => 'dummy option value 22',
                    'title' => 'dummy option title 22',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 3',
            'title' => 'dummy poll title 3',
            'options' => [
                [
                    'id' => 'option 31',
                    'value' => 'dummy option value 31',
                    'title' => 'dummy option title 31',
                ],
                [
                    'id' => 'option 32',
                    'value' => 'dummy option value 32',
                    'title' => 'dummy option title 32',
                ],
            ],
        ],
    ]);
    $deletePollRequest = new DeletePollRequest();
    $deletePollUseCase = new DeletePollUseCase($pollStore);

    // when
    $exception = null;
    try {
        $deletePollUseCase($deletePollRequest);
    } catch (BadRequestException $exception) {
    }

    // then
    expect($exception)->not()->toBeNull('BadRequestException not thrown.');
    expect($exception->getMessage())->toBe('Bad Request: missing id');
});
