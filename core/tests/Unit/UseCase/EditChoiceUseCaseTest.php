<?php

declare(strict_types=1);

use App\Domain\Poll\Exception\BadRequestException;
use App\Domain\Poll\UseCase\EditChoice\EditChoiceRequest;
use App\Domain\Poll\UseCase\EditChoice\EditChoiceUseCase;

test('can edit a choice', function () {
    // given
    given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'dummy option id 11',
                    'value' => 'dummy value 11',
                    'title' => 'dummy title 11',
                ],
                [
                    'id' => 'dummy option id 12',
                    'value' => 'dummy value 12',
                    'title' => 'dummy title 12',
                ],
                [
                    'id' => 'dummy option id 13',
                    'value' => 'dummy value 13',
                    'title' => 'dummy title 13',
                ],
            ],
        ],
    ]);
    $choiceStore = given_there_are_choices_in_database([
        [
            'id' => 'dummy choice id 1',
            'pollId' => 'dummy poll id 1',
            'participant' => 'sarah croche',
            'chosenOptionIds' => [
                'dummy option id 11',
            ],
        ],
    ]);
    $editChoiceUseCase = new EditChoiceUseCase($choiceStore);
    $editChoiceRequest = new EditChoiceRequest();
    $editChoiceRequest->choiceId = 'dummy choice id 1';
    $editChoiceRequest->chosenOptionIds = ['dummy option id 12', 'dummy option id 13'];

    // when
    $response = $editChoiceUseCase($editChoiceRequest);

    // then
    expectResponseToBe(
        response: $response,
        statusCode: 204,
        content: null,
    );
    $choice = $choiceStore->findById('dummy choice id 1');
    expect($choice->getChosenOptionIds())->toBe(['dummy option id 12', 'dummy option id 13']);
});

test('cannot edit a choice with missing values', function () {
    // given
    given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'dummy option id 11',
                    'value' => 'dummy value 11',
                    'title' => 'dummy title 11',
                ],
                [
                    'id' => 'dummy option id 12',
                    'value' => 'dummy value 12',
                    'title' => 'dummy title 12',
                ],
                [
                    'id' => 'dummy option id 13',
                    'value' => 'dummy value 13',
                    'title' => 'dummy title 13',
                ],
            ],
        ],
    ]);
    $choiceStore = given_there_are_choices_in_database([
        [
            'id' => 'dummy choice id 1',
            'pollId' => 'dummy poll id 1',
            'participant' => 'sarah croche',
            'chosenOptionIds' => [
                'dummy option id 11',
            ],
        ],
    ]);
    $editChoiceUseCase = new EditChoiceUseCase($choiceStore);
    $editChoiceRequest = new EditChoiceRequest();

    // when
    $exception = null;
    try {
        $editChoiceUseCase($editChoiceRequest);
    } catch (BadRequestException $exception) {
    }

    // then
    expect($exception)->not()->toBeNull('BadRequestException not thrown.');
    expect($exception->getMessage())->toBe('Bad Request: missing choiceId, missing chosenOptionIds');
});
