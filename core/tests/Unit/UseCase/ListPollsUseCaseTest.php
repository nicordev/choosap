<?php

declare(strict_types=1);

use App\Domain\Poll\UseCase\ListPolls\ListPollsRequest;
use App\Domain\Poll\UseCase\ListPolls\ListPollsUseCase;

test('can list polls', function () {
    // given
    $pollStore = given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'option 11',
                    'value' => 'dummy value 11',
                    'title' => 'dummy title 11',
                ],
                [
                    'id' => 'option 12',
                    'value' => 'dummy value 12',
                    'title' => 'dummy title 12',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 2',
            'title' => 'dummy poll title 2',
            'options' => [
                [
                    'id' => 'option 21',
                    'value' => 'dummy value 21',
                    'title' => 'dummy title 21',
                ],
                [
                    'id' => 'option 22',
                    'value' => 'dummy value 22',
                    'title' => 'dummy title 22',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 3',
            'title' => 'dummy poll title 3',
            'options' => [
                [
                    'id' => 'option 31',
                    'value' => 'dummy value 31',
                    'title' => 'dummy title 31',
                ],
                [
                    'id' => 'option 32',
                    'value' => 'dummy value 32',
                    'title' => 'dummy title 32',
                ],
            ],
        ],
    ]);
    $listPollsUseCase = new ListPollsUseCase($pollStore);
    $listPollsRequest = new ListPollsRequest();

    // when
    $response = $listPollsUseCase($listPollsRequest);

    // then
    expectResponseToBe(
        response: $response,
        statusCode: 200,
        content: [
            [
                'id' => 'dummy poll id 1',
                'title' => 'dummy poll title 1',
                'options' => [
                    [
                        'id' => 'option 11',
                        'value' => 'dummy value 11',
                        'title' => 'dummy title 11',
                    ],
                    [
                        'id' => 'option 12',
                        'value' => 'dummy value 12',
                        'title' => 'dummy title 12',
                    ],
                ],
            ],
            [
                'id' => 'dummy poll id 2',
                'title' => 'dummy poll title 2',
                'options' => [
                    [
                        'id' => 'option 21',
                        'value' => 'dummy value 21',
                        'title' => 'dummy title 21',
                    ],
                    [
                        'id' => 'option 22',
                        'value' => 'dummy value 22',
                        'title' => 'dummy title 22',
                    ],
                ],
            ],
            [
                'id' => 'dummy poll id 3',
                'title' => 'dummy poll title 3',
                'options' => [
                    [
                        'id' => 'option 31',
                        'value' => 'dummy value 31',
                        'title' => 'dummy title 31',
                    ],
                    [
                        'id' => 'option 32',
                        'value' => 'dummy value 32',
                        'title' => 'dummy title 32',
                    ],
                ],
            ],
        ],
    );
});

test('can list polls paginated', function () {
    // given
    $pollStore = given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'option 11',
                    'value' => 'dummy value 11',
                    'title' => 'dummy title 11',
                ],
                [
                    'id' => 'option 12',
                    'value' => 'dummy value 12',
                    'title' => 'dummy title 12',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 2',
            'title' => 'dummy poll title 2',
            'options' => [
                [
                    'id' => 'option 21',
                    'value' => 'dummy value 21',
                    'title' => 'dummy title 21',
                ],
                [
                    'id' => 'option 22',
                    'value' => 'dummy value 22',
                    'title' => 'dummy title 22',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 3',
            'title' => 'dummy poll title 3',
            'options' => [
                [
                    'id' => 'option 31',
                    'value' => 'dummy value 31',
                    'title' => 'dummy title 31',
                ],
                [
                    'id' => 'option 32',
                    'value' => 'dummy value 32',
                    'title' => 'dummy title 32',
                ],
            ],
        ],
    ]);
    $listPollsUseCase = new ListPollsUseCase($pollStore);
    $listPollsRequest = new ListPollsRequest();
    $listPollsRequest->offset = 1;
    $listPollsRequest->limit = 2;

    // when
    $response = $listPollsUseCase($listPollsRequest);

    // then
    expectResponseToBe(
        response: $response,
        statusCode: 200,
        content: [
            [
                'id' => 'dummy poll id 2',
                'title' => 'dummy poll title 2',
                'options' => [
                    [
                        'id' => 'option 21',
                        'value' => 'dummy value 21',
                        'title' => 'dummy title 21',
                    ],
                    [
                        'id' => 'option 22',
                        'value' => 'dummy value 22',
                        'title' => 'dummy title 22',
                    ],
                ],
            ],
            [
                'id' => 'dummy poll id 3',
                'title' => 'dummy poll title 3',
                'options' => [
                    [
                        'id' => 'option 31',
                        'value' => 'dummy value 31',
                        'title' => 'dummy title 31',
                    ],
                    [
                        'id' => 'option 32',
                        'value' => 'dummy value 32',
                        'title' => 'dummy title 32',
                    ],
                ],
            ],
        ],
    );
});
