<?php

use App\Domain\Poll\Exception\BadRequestException;
use App\Domain\Poll\UseCase\SavePoll\SavePollRequest;
use App\Domain\Poll\UseCase\SavePoll\SavePollUseCase;
use App\Tests\InMemory\InMemoryPollStore;
use App\Tests\Mock\IdFactoryMock;

test('can save a poll', function () {
    // given
    $pollStore = new InMemoryPollStore([]);
    $idFactory = new IdFactoryMock(['dummy id 1']);
    $savePollRequest = new SavePollRequest();
    $savePollRequest->title = 'dummy title';
    $savePollRequest->options = [
        [
            'title' => 'dummy option title 1',
            'value' => 'dummy option value 1',
        ],
        [
            'title' => 'dummy option title 2',
            'value' => 'dummy option value 2',
        ],
    ];
    $savePollUseCase = new SavePollUseCase($pollStore, $idFactory);

    // when
    $response = $savePollUseCase($savePollRequest);

    // then
    expectResponseToBe(
        response: $response,
        statusCode: 201,
        content: ['id' => $idFactory->getCurrentId()],
    );
    expect($pollStore->pollWithTitleExists($savePollRequest->title))->toBeTrue();
});

test('cannot save a poll without mandatory parameters title, options', function () {
    // given
    $pollStore = new InMemoryPollStore([]);
    $idFactory = new IdFactoryMock(['dummy id 1']);
    $savePollRequest = new SavePollRequest();
    $savePollUseCase = new SavePollUseCase($pollStore, $idFactory);

    // when
    $exception = null;
    try {
        $savePollUseCase($savePollRequest);
    } catch (BadRequestException $exception) {
    }

    // then
    expect($exception)->not()->toBeNull('BadRequestException not thrown.');
    expect($exception->getMessage())->toBe('Bad Request: missing title, missing options');
    expect($pollStore->pollExists('dummy id 1'))->toBeFalse();
});

test('cannot save a poll with invalid option', function () {
    // given
    $pollStore = new InMemoryPollStore([]);
    $idFactory = new IdFactoryMock(['dummy id 1']);
    $savePollRequest = new SavePollRequest();
    $savePollRequest->title = 'dummy title';
    $savePollRequest->options = [
        [
            'value' => 'dummy option value 1',
        ],
        [
            'title' => 'dummy option title 2',
        ],
        [
            'title' => '',
            'value' => '',
        ],
        [
            'title' => 'dummy option title 2',
            'value' => 'dummy option value 2',
        ],
    ];
    $savePollUseCase = new SavePollUseCase($pollStore, $idFactory);

    // when
    $exception = null;
    try {
        $savePollUseCase($savePollRequest);
    } catch (BadRequestException $exception) {
    }

    // then
    expect($exception)->not()->toBeNull('BadRequestException not thrown.');
    expect($exception->getMessage())->toBe('Bad Request: missing option title');
    expect($pollStore->pollExists('dummy id 1'))->toBeFalse();
});
