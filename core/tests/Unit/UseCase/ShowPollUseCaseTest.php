<?php

declare(strict_types=1);

use App\Domain\Poll\Exception\BadRequestException;
use App\Domain\Poll\Exception\PollNotFoundException;
use App\Domain\Poll\UseCase\ShowPoll\ShowPollRequest;
use App\Domain\Poll\UseCase\ShowPoll\ShowPollUseCase;

test('can show a poll', function () {
    // given
    $pollStore = given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'option 11',
                    'value' => 'dummy value 11',
                    'title' => 'dummy title 11',
                ],
                [
                    'id' => 'option 12',
                    'value' => 'dummy value 12',
                    'title' => 'dummy title 12',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 2',
            'title' => 'dummy poll title 2',
            'options' => [
                [
                    'id' => 'option 21',
                    'value' => 'dummy value 21',
                    'title' => 'dummy title 21',
                ],
                [
                    'id' => 'option 22',
                    'value' => 'dummy value 22',
                    'title' => 'dummy title 22',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 3',
            'title' => 'dummy poll title 3',
            'options' => [
                [
                    'id' => 'option 31',
                    'value' => 'dummy value 31',
                    'title' => 'dummy title 31',
                ],
                [
                    'id' => 'option 32',
                    'value' => 'dummy value 32',
                    'title' => 'dummy title 32',
                ],
            ],
        ],
    ]);
    $showPollUseCase = new ShowPollUseCase($pollStore);
    $showPollRequest = new ShowPollRequest();
    $showPollRequest->id = 'dummy poll id 2';

    // when
    $response = $showPollUseCase($showPollRequest);

    // then
    expectResponseToBe(
        response: $response,
        statusCode: 200,
        content: [
            'id' => 'dummy poll id 2',
            'title' => 'dummy poll title 2',
            'options' => [
                [
                    'id' => 'option 21',
                    'value' => 'dummy value 21',
                    'title' => 'dummy title 21',
                ],
                [
                    'id' => 'option 22',
                    'value' => 'dummy value 22',
                    'title' => 'dummy title 22',
                ],
            ],
        ],
    );
});

test('cannot show an unknown poll', function () {
    // given
    $pollStore = given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'option 11',
                    'value' => 'dummy value 11',
                    'title' => 'dummy title 11',
                ],
                [
                    'id' => 'option 12',
                    'value' => 'dummy value 12',
                    'title' => 'dummy title 12',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 2',
            'title' => 'dummy poll title 2',
            'options' => [
                [
                    'id' => 'option 21',
                    'value' => 'dummy value 21',
                    'title' => 'dummy title 21',
                ],
                [
                    'id' => 'option 22',
                    'value' => 'dummy value 22',
                    'title' => 'dummy title 22',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 3',
            'title' => 'dummy poll title 3',
            'options' => [
                [
                    'id' => 'option 31',
                    'value' => 'dummy value 31',
                    'title' => 'dummy title 31',
                ],
                [
                    'id' => 'option 32',
                    'value' => 'dummy value 32',
                    'title' => 'dummy title 32',
                ],
            ],
        ],
    ]);
    $showPollUseCase = new ShowPollUseCase($pollStore);
    $showPollRequest = new ShowPollRequest();
    $showPollRequest->id = 'unknown poll';

    // when
    $exception = null;
    try {
        $showPollUseCase($showPollRequest);
    } catch (PollNotFoundException $exception) {
    }

    // then
    expect($exception)->not()->toBeNull();
    expect($exception->getMessage())->toBe('Poll with id unknown poll not found.');
});

test('cannot show a poll without complete request', function () {
    // given
    $pollStore = given_there_are_polls_in_database([
        [
            'id' => 'dummy poll id 1',
            'title' => 'dummy poll title 1',
            'options' => [
                [
                    'id' => 'option 11',
                    'value' => 'dummy value 11',
                    'title' => 'dummy title 11',
                ],
                [
                    'id' => 'option 12',
                    'value' => 'dummy value 12',
                    'title' => 'dummy title 12',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 2',
            'title' => 'dummy poll title 2',
            'options' => [
                [
                    'id' => 'option 21',
                    'value' => 'dummy value 21',
                    'title' => 'dummy title 21',
                ],
                [
                    'id' => 'option 22',
                    'value' => 'dummy value 22',
                    'title' => 'dummy title 22',
                ],
            ],
        ],
        [
            'id' => 'dummy poll id 3',
            'title' => 'dummy poll title 3',
            'options' => [
                [
                    'id' => 'option 31',
                    'value' => 'dummy value 31',
                    'title' => 'dummy title 31',
                ],
                [
                    'id' => 'option 32',
                    'value' => 'dummy value 32',
                    'title' => 'dummy title 32',
                ],
            ],
        ],
    ]);
    $showPollUseCase = new ShowPollUseCase($pollStore);
    $showPollRequest = new ShowPollRequest();

    // when
    $exception = null;
    try {
        $showPollUseCase($showPollRequest);
    } catch (BadRequestException $exception) {
    }

    // then
    expect($exception)->not()->toBeNull('BadRequestException not thrown.');
    expect($exception->getMessage())->toBe('Bad Request: missing id');
});
